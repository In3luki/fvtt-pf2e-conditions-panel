const path = require("path");
const fs = require('fs-extra');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

function getFoundryConfig() {
    const configPath = path.resolve(process.cwd(), 'foundryconfig.json');
    let config;

    if (fs.existsSync(configPath)) {
        config = fs.readJSONSync(configPath);
        return config;
    }
}

module.exports = (env, argv) => {
    const config = {
        context: __dirname,
        entry: { 
            index: path.resolve(__dirname, "src", "main.ts") 
        },
        mode: "none",
        watchOptions: {
            ignored: /node_modules/,
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'ts-loader',
                    exclude: /node_modules/,
                },
                {
                    test: /\.scss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                url: false,
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true,
                            }
                        }
                    ],
                    exclude: /node_modules/,
                }
            ],
        },
        plugins: [
            new CopyWebpackPlugin({
                patterns: [
                    { from: './src/templates', to: 'templates'},
                    { from: 'module.json', to: 'module.json' },
                ],
            },
                {
                 writeToDisk: true 
                }
            ),
            new MiniCssExtractPlugin({
                filename: 'styles/style.css'
            }),
        ],
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },
        output: {
            path: path.join(__dirname, 'dist'),
            filename: "main.bundle.js",
        },
    };

    const foundryConfig = getFoundryConfig();
    if (foundryConfig !== undefined)
        config.output.path = path.join(foundryConfig.dataPath, 'Data', 'modules', foundryConfig.moduleName);

    if (argv.mode === 'production') {
        config.optimization = {
            minimize: true,
            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        mangle:false,
                    }
                }),
            ],
        };
    } else {
        config.devtool = 'inline-source-map';
        config.watch = true;
    }

    return config;
};
