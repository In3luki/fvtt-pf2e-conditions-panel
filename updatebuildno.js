const yargs = require('yargs');
const fs = require('fs');

const argv = yargs
    .option('buildno', {
        type: 'number',
        description: 'specifies the build number to be updated (CI_PIPELINE_IID)'
    })
    .option('branch', {
        'type': 'string',
        description: 'specifies the branch (CI_COMMIT_BRANCH)'
    })
    .option('gitlabpath', {
        type: 'string',
        description: 'The path on gitlab where this branch is stored (CI_PROJECT_PATH)'
    })
    .demandOption(['branch', 'buildno'])
    .argv;

const moduleRaw = fs.readFileSync('module.json');
let _module = JSON.parse(moduleRaw);

_module.version = `${_module.version}.${argv.buildno}`;
_module.url = `https://gitlab.com/${argv.gitlabpath}`;
_module.manifest = `https://gitlab.com/${argv.gitlabpath}/-/jobs/artifacts/${argv.branch}/raw/module.json?job=build`;
_module.download = `https://gitlab.com/${argv.gitlabpath}/-/jobs/artifacts/${argv.branch}/raw/pf2e-conditions-panel.zip?job=build`;

fs.writeFileSync('module.json', JSON.stringify(_module, null, 2));

console.log(_module.manifest);
