import { PF2eConditionsPanel } from './module/PF2eConditionsPanel';

require('./styles/style.scss');

Hooks.once('setup', () => {
    console.log('PF2EConditionsPanel | Initialzing PF2e Conditions Panel');

    game.settings.register('pf2e-conditions-panel', 'gmOnly', {
        name: "GM Only",
        hint: "Restrict the panel to GM users.",
        scope: 'world',
        config: true,
        default: true,
        type: Boolean,
        onChange: () => {
            window.location.reload();
        }
    });

    game.settings.register('pf2e-conditions-panel', 'conditionalStyle', {
        name: "Conditional Effects Style",
        hint: "Set how conditionally applied effects are displayed.",
        scope: 'client',
        config: true,
        default: "red",
        type: String,
        choices: {
            red: "Red Border",
            grey: "Greyed Out"
        },
        onChange: (choice: string) => {
            panel.conditionalStyle = choice;
            if (panel.rendered)
                panel.render(true);
        }
    });

    game.settings.register('pf2e-conditions-panel', 'windowPosition', {
        name: "Window Position",
        scope: 'client',
        config: false,
        default: {
            width: 395,
            height: 560
        },
        type: Object
    });

    const panel = new PF2eConditionsPanel();
    panel.conditionalStyle = game.settings.get('pf2e-conditions-panel', 'conditionalStyle');

    const controlButton = {
        name: "conditionsPanel",
        title: "Conditions Panel",
        icon: "far fa-frown-open",
        visible: false,
        button: true,
        onClick: () => panel.render(true)
    }

    const gmOnly = game.settings.get('pf2e-conditions-panel', 'gmOnly');
    Hooks.on('getSceneControlButtons', (controls: any) => {
        const tokenButtons = controls.find((c: any) => c.name === 'token');
        if (tokenButtons) {
            controlButton.visible = (game.user.isGM || !gmOnly);
            tokenButtons.tools.push(controlButton);
        }
    });
});
