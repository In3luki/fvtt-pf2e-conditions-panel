/* global Application */
import { ConditionData, CompactConditionData, TokenConditionData, } from './DataDefinitions';

declare let PF2eStatusEffects: any;
declare let PF2e: any;

export class PF2eConditionsPanel extends Application {
    conditionsCache: ConditionData[];
    conditionalStyle: string;
    messageTimeout: any;
    controlledTimeout: any;
    controlledTokens: Token[];
    changedConditions = false;
    disabled = true;
    different = false;

    constructor() {
        super();

        this.init();
    }

    static get defaultOptions() {
        const options: any = {
            title: "PF2e Conditions Panel",
            template: "modules/pf2e-conditions-panel/templates/conditions-panel.html",
            classes: ["conditions-panel"],
            resizable: true
        };

        const position = game.settings.get('pf2e-conditions-panel', 'windowPosition');
        if (position) {
            options.width = position.width;
            options.height = position.height;
            options.top = position.top;
            options.left = position.left;
        }

        return mergeObject(super.defaultOptions, options);
    }

    init() {
        Hooks.on('controlToken', () => {
            if (this.rendered) {
                if (this.controlledTimeout) {
                    clearTimeout(this.controlledTimeout);
                }
                this.controlledTimeout = setTimeout(() => {
                    this.getControlledTokens();
                }, 100);
            }
            if (this.changedConditions) {
                clearTimeout(this.messageTimeout);
                this.generateChatMessages();
            }
        });
        Hooks.on('updateToken', (scene: Scene, tokenData: any, data: any) => {
            if (this.controlledTokens?.length === 1 && this.controlledTokens[0]?.id === tokenData?._id) {
                if (data?.effects && this.rendered) {
                    this.render();
                }
            }
        });
    }

    _onResize() {
        game.settings.set('pf2e-conditions-panel', 'windowPosition', this.position);
    }

    async close() {
        game.settings.set('pf2e-conditions-panel', 'windowPosition', this.position);
        super.close();
    }

    async getData() {
        const data: any = {};

        if (!this.conditionsCache) {
            await this.cacheConditions();
        }

        this.getControlledTokens();
        const tokenConditions = this.getTokenConditions();
        data.conditions = this.getConditionData(tokenConditions);

        data.conditionalStyle = this.conditionalStyle;
        return data;
    }

    async cacheConditions() {
        const conditionsCompendium = game.packs.find(p => p.metadata.name === 'conditionitems');
        this.conditionsCache = await conditionsCompendium.getContent();
        console.log('PF2e Conditions Panel | Conditions cached.');
    }

    getControlledTokens() {
        this.controlledTokens = canvas.tokens.controlled;
        this.different = false;

        if (this.controlledTokens.length)
            this.disabled = false;
        else
            this.disabled = true;

        if (this.rendered)
            this.render();
    }

    getConditionData(tokenConditions: TokenConditionData[]): CompactConditionData[] {
        const allConditions: CompactConditionData[] = this.conditionsCache.filter((c: ConditionData) => c.data.data.group !== 'attitudes').map((condition: ConditionData) => {
            const entry: CompactConditionData = {
                name: condition.name,
                statusName: condition.data.data.hud.statusName,
                isValued: condition.data.data.value.isValued,
                img: condition.img
            };

            const activeCondition = tokenConditions.find((c: TokenConditionData) => c.name === condition.name);
            if (activeCondition) {
                entry.active = true;
                entry.hasParent = activeCondition.hasParent;
                entry.value = activeCondition.value;
            }

            return entry;
        });

        return allConditions;
    }

    getTokenConditions(): TokenConditionData[] {
        if (this.controlledTokens.length === 0) return [];

        const allTokenConditions: TokenConditionData[][] = [];
        for (const token of this.controlledTokens) {
            const currentTokenConditions: TokenConditionData[] = token.actor.items.filter((i: ConditionData) => i.type === 'condition' && i.data.flags.pf2e?.condition && i.data.data.active).map((condition: ConditionData) => {
                return {
                    name: condition.name,
                    value: condition.data.data.value.value,
                    hasParent: (condition.data.data.references.parent !== undefined)
                }
            });
            allTokenConditions.push(currentTokenConditions);
        }

        let tokenConditions: TokenConditionData[] = [];
        let firstPass = true;
        for (const singleTokenConditions of allTokenConditions) {
            if (firstPass) {
                tokenConditions = singleTokenConditions;
                firstPass = false;
            } else {
                if (singleTokenConditions.length !== tokenConditions.length) {
                    tokenConditions = [];
                    this.different = true;
                    this.disabled = true;
                } else {
                    for (const condition of singleTokenConditions) {
                        if (!tokenConditions.some((c: TokenConditionData) => c.name === condition.name && c.value === condition.value && c.hasParent === condition.hasParent)) {
                            tokenConditions = [];
                            this.different = true;
                            this.disabled = true;
                            break;
                        }
                    }
                }

                if (this.different)
                  break;
            }
        }

        return tokenConditions;
    }

    async handleClickEvents(event: any) {
        const updates = [];
        for (const token of this.controlledTokens) {
            if(event?.currentTarget?.dataset?.isValued === 'true'){
                updates.push(PF2eStatusEffects._setStatusValue.call(token, event));
            } else {
                updates.push(PF2eStatusEffects._toggleStatus.call(token, event));
            }
        }
        await Promise.all(updates);

        this.changedConditions = true;

        if (this.messageTimeout) {
            clearTimeout(this.messageTimeout);
        }
        this.messageTimeout = setTimeout(() => {
            this.generateChatMessages();
        }, 2000);
        this.render();
    }

    activateListeners(html: JQuery) {
        super.activateListeners(html);

        if (!this.disabled) {
            html.on("click", ".pf2e-effect-control", (event) => {
                this.handleClickEvents(event);
            });
            html.on("contextmenu", ".pf2e-effect-control", async (event) => {
                this.handleClickEvents(event);
            });
        }
        this.setWindowTitle(html);
    }

    setWindowTitle(html: JQuery) {
        const title = html.offsetParent().find('.window-title');

        let titleText = 'No Tokens Selected';

        if (this.different) {
            titleText = 'Different Conditions';
        } else if (this.disabled) {
            titleText = 'No Tokens Selected';
        } else if (this.controlledTokens.length > 1) {
            titleText = 'Multiple Tokens';
        } else if (this.controlledTokens.length === 1) {
            titleText = this.controlledTokens[0].name;
        }

        title.text(`${title.text()} [${titleText}]`);
    }

    generateChatMessages() {
        this.changedConditions = false;
        for (const t of this.controlledTokens) {
            if ((t as any)?.statusEffectChanged === true) {
                 (t as any).statusEffectChanged = false;
                this._createChatMessage(t);
            }
        }
    }

    _createChatMessage(token: any) {
        let statusEffectList = ''
 
        // Get the active applied conditions.
        // Iterate the list to create the chat and bubble chat dialog.

        for (const condition of token.actor.data.items.filter((i: ConditionData) => i.flags.pf2e?.condition && i.data.active && i.type === 'condition')) {
            statusEffectList += `
                <li><img src="${`${CONFIG.PF2E.statusEffects.effectsIconFolder + condition.data.hud.statusName }.${ CONFIG.PF2E.statusEffects.effectsIconFileType}`}" title="${PF2e.DB.condition[condition.data.hud.statusName].summary}">
                    <span class="statuseffect-li">
                        <span class="statuseffect-li-text">${condition.name} ${(condition.data.value.isValued) ? condition.data.value.value : ''}</span>
                        <div class="statuseffect-rules"><h2>${condition.name}</h2>${condition.data.description.value}</div>
                    </span>
                </li>`;
        }

        if (statusEffectList === '') {
            // No updates
            return;
        }

        const message = `
            <div class="dice-roll">
                <div class="dice-result">
                    <div class="dice-total statuseffect-message">
                        <ul>${statusEffectList}</ul>
                    </div>
                </div>
            </div>
        `;

        const chatData: any = {
            user: game.user._id,
            speaker: { alias: `${token.name}'s status effects:` },
            content: message,
            type: CONST.CHAT_MESSAGE_TYPES.OTHER
        }
        ChatMessage.create(chatData);
    }
}
