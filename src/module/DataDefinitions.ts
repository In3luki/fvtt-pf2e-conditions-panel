export interface CompactConditionData {
    name: string,
    statusName: string,
    img: string,
    isValued: boolean,
    value?: number,
    active?: boolean,
    hasParent?: boolean
}

export interface TokenConditionData {
    name: string,
    value: number,
    hasParent: boolean
}

export interface ItemDescriptionData {
    description: {
        value: string;
        chat: string;
        unidentified: string;
    };
    source: {
        value: string;
    };
    traits: {
        value: string;
    };
    rarity: {
        value: 'common' | 'uncommon' | 'rare' | 'unique';
    };
    usage: {
        value: string;
    };
    data?: {
        description: {
            value: string;
            chat: string;
            unidentified: string;
        };
        source: {
            value: string;
        };
        traits: {
            value: string;
        };
        rarity: {
            value: 'common' | 'uncommon' | 'rare' | 'unique';
        };
        usage: {
            value: string;
        };        
    };
}

export interface StatusDetailsData {
    active: boolean,
    removable: boolean,
    references: {
        parent: {
            id: string,
            type: 'status' | 'condition' | 'feat' | 'weapon' | 'armor' | 'consumable' | 'equipment' | 'spell'
        },
        children: [{
            id: string,
            type: 'condition'
        }],
        overriddenBy: [{
            id: string,
            type: 'condition'
        }],
        overrides: [{
            id: string,
            type: 'condition'
        }],
        /**
         * This status is immune, and thereby inactive, from the following list.
         */
        immunityFrom: [{
            id: string,
            type: 'status' | 'condition' | 'feat' | 'weapon' | 'armor' | 'consumable' | 'equipment' | 'spell'
        }]
    },
    hud: {
        statusName: string,
        img: {
            useStatusName: boolean,
            value: string
        },
        selectable: boolean
    },
    duration: {
        perpetual: boolean,
        value: number,
        text: string
    },
    modifiers: [{
        type: 'ability' | 'proficiency' | 'status' | 'circumstance' | 'item' | 'untyped',
        name: string,
        group: string,
        value?: number,
    }],
    flags: {
        pf2e: {
            condition: boolean,
        },
    },
    data?: {
        active: boolean,
        removable: boolean,
        references: {
            parent: {
                id: string,
                type: 'status' | 'condition' | 'feat' | 'weapon' | 'armor' | 'consumable' | 'equipment' | 'spell'
            },
            children: [{
                id: string,
                type: 'condition'
            }],
            overriddenBy: [{
                id: string,
                type: 'condition'
            }],
            overrides: [{
                id: string,
                type: 'condition'
            }],
            /**
             * This status is immune, and thereby inactive, from the following list.
             */
            immunityFrom: [{
                id: string,
                type: 'status' | 'condition' | 'feat' | 'weapon' | 'armor' | 'consumable' | 'equipment' | 'spell'
            }]
        },
        hud: {
            statusName: string,
            img: {
                useStatusName: boolean,
                value: string
            },
            selectable: boolean
        },
        duration: {
            perpetual: boolean,
            value: number,
            text: string
        },
        modifiers: [{
            type: 'ability' | 'proficiency' | 'status' | 'circumstance' | 'item' | 'untyped',
            name: string,
            group: string,
            value?: number,
        }]
    }
}

export interface ConditionDetailsData {
    base: string,
    group: string,
    value: {
        isValued: boolean,
        immutable: boolean,
        value: number,
        modifiers: [{
            value: number,
            source: string
        }]
    },
    sources: {
        hud: boolean
    },
    alsoApplies: {
        linked: [{
            condition: string,
            value?: number
        }],
        unlinked: [{
            condition: string,
            value?: number
        }]
    },
    overrides: [],
    data?: {
        base: string,
        group: string,
        value: {
            isValued: boolean,
            immutable: boolean,
            value: number,
            modifiers: [{
                value: number,
                source: string
            }]
        },
        sources: {
            hud: boolean
        },
        alsoApplies: {
            linked: [{
                condition: string,
                value?: number
            }],
            unlinked: [{
                condition: string,
                value?: number
            }]
        },
        overrides: []
    }
}

export interface ConditionData extends BaseEntityData<ItemDescriptionData & StatusDetailsData & ConditionDetailsData> {
    type: 'condition'
}